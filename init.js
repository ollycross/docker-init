const inquirer = require('inquirer');
const fs = require('fs');
const path = require('path');
const nodegit = require('nodegit');
const rimraf = require('rimraf');
const mergeYaml = require('merge-yaml-cli');

const dockerBaseGitAddr = 'https://gitlab.com/ollycross/docker-base';

const randomString = (length = 12) =>
  [...Array(length)].map((i) => (~~(Math.random() * 36)).toString(36)).join('');

const resolvePath = (filePath) =>
  filePath[0] === '~'
    ? path.join(process.env.HOME, filePath.slice(1))
    : filePath;

const applications = [
  {
    slug: 'default',
    label: 'None (blank application)',
  },
  {
    slug: 'wordpress',
    label: 'Wordpress',
    callback: (projectPath, templatePath) => {
      fs.copyFileSync(templatePath('.htaccess'), projectPath('/app/html/.htaccess'));
      fs.copyFileSync(templatePath('wp-config.php'), projectPath('/app/html/wp-config.php'));
    }
  },
];

const questions = [
  {
    name: 'projectName',
    message: "Enter your project's name",
    validate: (val) =>
      val.match(/^[a-zA-Z\s\-]+$/)
        ? true
        : 'Name must be only letters, spaces, or dashes',
  },
  {
    name: 'projectPath',
    message: 'Enter the project path',
    default: (answers) => `${process.cwd()}/${answers.projectName}`,
    filter: (val) => resolvePath(val),
    transformer: val => val,
    validate: (val) => (fs.existsSync(resolvePath(val)) ? 'Path exists' : true),
  },
  {
    name: 'composerName',
    message: 'Composer name (vendor/project)',
    default: (answers) => `ollyollyolly/${answers.projectName}`,
  },
  {
    name: 'phpVersion',
    message: 'PHP version to use',
    default: '7.4',
  },
  {
    name: 'dbRootPassword',
    description: 'Root password for DB',
    default: randomString(12),
  },
  {
    name: 'dbName',
    description: 'DB Schema',
    default: (answers) => answers.projectName,
  },
  {
    name: 'dbUsername',
    description: (answers) => `Username for ${answers.dbName} schema`,
    default: (answers) => answers.dbName,
  },
  {
    name: 'dbPassword',
    message: (answers) => `Password for ${answers.dbUsername} user`,
    default: randomString(12),
  },

  {
    name: 'applicationSlug',
    message: 'Pick application type',
    type: 'list',
    choices: applications.map(application => ({
      value: application.slug,
      name: application.label
    })),
    default: 'default',
  },
];



inquirer.prompt(questions).then((answers) => {
  const cloneRepo = async (projectPath) => {
    console.log(`Cloning ${dockerBaseGitAddr} into ${projectPath}`);
    return await nodegit
      .Clone(dockerBaseGitAddr, projectPath)
      .then(() => {
        console.log('Done!');
      })
      .catch(console.error);
  };

  const {
    composerName,
    projectName,
    phpVersion,
    dbRootPassword,
    dbName,
    dbUsername,
    dbPassword,
  } = answers;

  const application = applications.find(app => app.slug === answers.applicationSlug);

  const projectPath = (append) =>
    `${answers.projectPath}${append ? `/${append}` : ''}`;

  const templatePath = (append) =>
    `${__dirname}/templates/${application.slug}${append ? `/${append}` : ''}`

  const creatEnvFile = () =>
    fs
      .readFileSync(`${__dirname}/templates/.env.template`)
      .toString()
      .replace('%PROJECT_NAME%', projectName)
      .replace('%PHP_VERSION%', phpVersion)
      .replace('%DB_ROOT_PASSWORD%', dbRootPassword)
      .replace('%DB_NAME%', dbName)
      .replace('%DB_USERNAME%', dbUsername)
      .replace('%DB_PASSWORD%', dbPassword);

  const createPackageJsonFile = () => fs
      .readFileSync(`${__dirname}/templates/package.json.template`)
      .toString()
      .replace('%PROJECT_NAME%', projectName)


  const createComposerFile = () =>
    fs
      .readFileSync(templatePath(`/composer.${application.slug}.json`))
      .toString()
      .replace('%COMPOSER_NAME%', composerName);

  const createDockerComposeFile = () => {
    const files =
      [
        `${__dirname}/docker/docker-compose.yml`
      ];
      const applicationYml = templatePath(`/docker-compose.${application.slug}.yml`);
      if(fs.existsSync(applicationYml)) {
        files.push(`${applicationYml}`);
      }

      return mergeYaml.merge(files);
  }

  cloneRepo(projectPath()).then(() => {
    fs.writeFileSync(projectPath('.env'), creatEnvFile());
    fs.writeFileSync(projectPath('package.json'), createPackageJsonFile());
    fs.writeFileSync(projectPath('app/composer.json'), createComposerFile());
    fs.writeFileSync(projectPath('docker-compose.yml'), createDockerComposeFile());

    rimraf.sync(projectPath('.git'));

    if(application.callback) {
      application.callback.call(this, projectPath, templatePath)
    }

    console.log('Done!');

  });
});

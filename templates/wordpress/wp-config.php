<?php

if (!defined('ABSPATH')) {
    die();
}

require_once sprintf('%s/../vendor/autoload.php', __DIR__);

if (file_exists(sprintf('%s/../.env', __DIR__))) {
    $dotenv = \Dotenv\Dotenv::createImmutable(sprintf('%s/..', __DIR__));
    $dotenv->load();
}

define('WP_ENV', getenv('WP_ENVIRONMENT') ?: 'production');

define('DB_NAME', getenv('WP_DB_NAME'));
define('DB_USER', getenv('WP_DB_USER'));
define('DB_PASSWORD', getenv('WP_DB_PASSWORD'));
define('DB_HOST', getenv('WP_DB_HOST'));

if (getenv('WP_DB_CHARSET')) {
    define('DB_CHARSET', getenv('WP_DB_CHARSET'));
}

if (getenv('WP_DB_COLLATE')) {
    define('DB_COLLATE', getenv('WP_DB_COLLATE'));
}

define('AUTH_KEY', getenv('WP_DB_AUTH_KEY'));
define('SECURE_AUTH_KEY', getenv('WP_SECURE_AUTH_KEY'));
define('LOGGED_IN_KEY', getenv('WP_LOGGED_IN_KEY'));
define('NONCE_KEY', getenv('WP_NONCE_KEY'));
define('AUTH_SALT', getenv('WP_AUTH_SALT'));
define('SECURE_AUTH_SALT', getenv('WP_SECURE_AUTH_SALT'));
define('LOGGED_IN_SALT', getenv('WP_LOGGED_IN_SALT'));
define('NONCE_SALT', getenv('WP_NONCE_SALT'));

if (getenv('WP_HOME')) {
    define('WP_HOME', getenv('WP_HOME'));
}

if (getenv('WP_SITEURL')) {
    define('WP_SITEURL', getenv('WP_SITEURL'));
}

$table_prefix = getenv('WP_TABLE_PREFIX') ?: 'wp_';

define('WP_CONTENT_DIR', sprintf('%s/assets', __DIR__));
define(
    'WP_CONTENT_URL',
    sprintf('%s/assets', defined('WP_HOME') ? WP_HOME : '')
);

if (getenv('WP_MULTISITE') === 'true') {
    define('WP_ALLOW_MULTISITE', true);
    // define('MULTISITE', true);
    // define('SUBDOMAIN_INSTALL', false);
    // define('DOMAIN_CURRENT_SITE', preg_replace('#^https?://#', '', WP_SITEURL));
    // define('PATH_CURRENT_SITE', '/');
    // define('SITE_ID_CURRENT_SITE', 1);
    // define('BLOG_ID_CURRENT_SITE', 1);
    // if (file_exists(sprintf('%s/sunrise.php', WP_CONTENT_DIR))) {
    //     define('SUNRISE', true);
    // }
    // if (isset($_SERVER['HTTP_HOST'])) {
    //     define('COOKIE_DOMAIN', $_SERVER['HTTP_HOST']);
    // }
}

if (getenv('WPMDB_LICENCE')) {
    define('WPMDB_LICENCE', getenv('WPMDB_LICENCE'));
}

define('WP_DEBUG', WP_ENV !== 'production');

if (class_exists('Kint')) {
    Kint::$enabled_mode = WP_DEBUG;
}

if (!defined('FS_METHOD')) {
    define('FS_METHOD', getenv('WP_FS_METHOD') ?: 'direct');
}

require_once ABSPATH . 'wp-settings.php';
